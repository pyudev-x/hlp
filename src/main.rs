use std::path::Path;
use std::process::{Command, exit};

mod util;
use std::fs::read_to_string;
mod token; use token::*;

fn main() {
    parse_stream(&mut parse_file(Path::new("code.txt")));
    exit(0);
}
fn parse_stream(stream: &mut Vec<Token>) {
    for (index, token) in stream.iter().enumerate() {
        macro_rules! eval_operator {
            ($op:tt) => {
                let n1 = stream.get(index-1);
                let n2 = stream.get(index+1);

                if let (Some(Token::Expr(Expression::Number(x))), Some(Token::Expr(Expression::Number(y)))) = (n1, n2) {
                    println!("{}", x $op y); 
                }
            };
        }

        match token {
            Token::Operator(x) => {
                match x {
                    Operator::ADD => {
                        eval_operator!(+);
                    }
                    Operator::SUB => {
                        eval_operator!(-);
                    }
                    Operator::MUL => {
                        eval_operator!(*);
                    }
                    Operator::DIV => {
                        eval_operator!(/);
                    }
                    Operator::MOD => {
                        eval_operator!(%);
                    }
                    Operator::EXP => {
                        let n1 = stream.get(index-1);
                        let n2 = stream.get(index+1);

                        if let (Some(Token::Expr(Expression::Number(x))), Some(Token::Expr(Expression::Number(y)))) = (n1, n2) {
                            println!("{}", x.powf(*y)); 
                        }
                    }
                }
            }
            Token::Print() => {
                let expr = stream.get(index+1);
                if let Some(Token::Expr(x)) = expr {
                    println!("{:?}", x);
                }
            }

            Token::Echo() => {
                let expr = stream.get(index+1);
                if let Some(Token::Expr(x)) = expr {
                    match x {
                        Expression::String(y) => {println!("{}", y);}
                        Expression::Number(y) => {println!("{}", y);}
                        Expression::Bool(y) => {println!("{}", y);}
                        _ => {println!("{:?}", x)} // debug print other expression types
                    }
                }
            }

            Token::System() => {
                let cmd = stream.get(index+1);
                if let Some(Token::Expr(Expression::String(x))) = cmd {
                    let split = x.split(" ").collect::<Vec<&str>>();
                    let cmd = &split[0];
                    let args = &split[1..];
                    
                    Command::new(cmd.to_string()).args(args).spawn().unwrap().wait().unwrap();
                }
            }

            Token::Future() => {
                let tkn = stream.get(index+1);
                if let Some(x) = tkn {
                    println!("{:?}", x);
                }
            }
            Token::Past() => {
                let tkn = stream.get(index-1);
                if let Some(x) = tkn {
                    println!("{:?}", x);
                }
            }

            Token::Equals() => {
                let n1 = stream.get(index-1);
                let n2 = stream.get(index+1);

                match (n1, n2) {
                    (Some(Token::Expr(Expression::String(x))), Some(Token::Expr(Expression::String(y)))) => {
                        println!("{}", x == y);
                    } 
                    (Some(Token::Expr(Expression::Number(x))), Some(Token::Expr(Expression::Number(y)))) => {
                        println!("{}", x == y);
                    } 
                    (Some(Token::Expr(Expression::Bool(x))), Some(Token::Expr(Expression::Bool(y)))) => {
                        println!("{}", x == y);
                    } 
                    _ => {
                        println!("Cannot match equality.")
                    }
                }
            }
            _ => {}
        }
    }
}


fn parse_file(path:&Path) -> Vec<Token> {
    let mut stream: Vec<Token> = vec![];
    let mut current_line = 0;

    if path.exists() {
        let code = read_to_string(path).unwrap();
        
        for line in code.lines() {
            current_line += 1;
            let line = line.to_string();

            let split = line.split(". ").collect::<Vec<&str>>();

            for x in split.clone() {
                if x.starts_with("//") || x.is_empty() {continue;}

                match x {
                    // Operators
                    "+" | "add" => {stream.push(Token::Operator(Operator::ADD));}
                    "-" | "sub" => {stream.push(Token::Operator(Operator::SUB));}
                    "*" | "mul" => {stream.push(Token::Operator(Operator::MUL));}
                    "/" | "div" => {stream.push(Token::Operator(Operator::DIV));}
                    "%" | "mod" => {stream.push(Token::Operator(Operator::MOD));}
                    "^" | "exp" | "pow" => {stream.push(Token::Operator(Operator::EXP));}
                    "==" | "equals" => {stream.push(Token::Equals());}

                    // Boolean Values
                    "true" | "yes" | "on" => {stream.push(Token::Expr(Expression::Bool(true)));}
                    "false" | "no" | "off" => {stream.push(Token::Expr(Expression::Bool(false)));}
                    
                    // Other
                    "unknown" | "null" => {stream.push(Token::Expr(Expression::Unknown(())));}

                    // "Functions"
                    "debug" | "log" => {stream.push(Token::Print());}
                    "echo" | "print" => {stream.push(Token::Echo());}
                    "system" | "exec" => {stream.push(Token::System());}
                    
                    // How do I explain these?
                    "future" | "forward" => {stream.push(Token::Future());}
                    "past" | "backward" => {stream.push(Token::Past());}

                    // Strings, Numbers, and Errors
                    _ => {
                        if !x.parse::<f32>().is_ok() {
                            if x.starts_with("\"") && x.ends_with("\"") {
                                stream.push(Token::Expr(Expression::String(x.replace("\"", ""))));
                            } else {
                                println!("ERROR: Invalid token(s) \"{:?}\" at line {}", split.clone(), current_line);
                                exit(1);
                            }
                        } else {
                            stream.push(Token::Expr(Expression::Number(x.parse::<f32>().unwrap())));
                        }
                    } 
                }
            }
        }
    }
        
    return stream;
}

#[allow(dead_code)]
#[derive(Debug)]
#[derive(PartialEq)]
pub enum Token {
    Operator(Operator), // An Operator
    Logic(LogicOperator), // An Logic Operator
    Expr(Expression), // An Expression
    Print(), // Debug Printing
    Echo(), // Print to Output
    System(), // Run System Commands
    Equals(), // Check Equality
    Future(), // THE F U T U R E
    Past() // THE P A S T
}

#[allow(dead_code)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum Operator {
    ADD, // Addition Operator
    SUB, // Subtraction Operator
    MUL, // Multiplication Operator
    DIV, // Division Operator
    MOD, // Modulus Operator
    EXP // Exponent Operator
}

#[allow(dead_code)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum Expression {
    Number(f32),
    String(String),
    Bool(bool),
    Unknown(())
}

#[allow(dead_code)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum LogicOperator {
    EQL, // Equals Operator
    NEQL // Not Equals Operator
}
